# Registration

#### Url (POST)

```url
/api/register 
```

#### Body


```json
{
  "name": "Full Name",
  "email": "example@email.com",
  "password": "password"
}
```


# Login

#### Url (POST)

```url
/api/login
```
#### Body


```json
{
  "email": "example@email.com",
  "password": "password"
}
```



# Product List

#### Url (GET)

```url
/api/products
```
#### Headers

```headers
Authorization : Bearer $token
```


# Product CSV Upload

#### Url (POST)

```url
/api/products/upload
```
#### Headers

```headers
Authorization : Bearer $token
```
#### Body


```

  upload_file : csv file

```


# Logout

#### Url (POST)

```url
/api/logout
```
#### Headers

```headers
Authorization : Bearer $token
```
