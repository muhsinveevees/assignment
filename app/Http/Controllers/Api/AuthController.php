<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use ApiHelper;

    public function login(Request $request)
    {
    	$this->validate($request, [
            'email' => ['required', 'string', 'email',],
            'password' => ['required', 'string'],
        ]);

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $data = $user->toArray();
            $token =  $user->createToken('leads')->plainTextToken;
            $data['token'] = $token;
            return $this->sendResponse(true,$data); 
        } 

        return $this->sendResponse(false, 'Invalid login', 'Unauthorised', 401);

    }

    public function register(Request $request)
    {
    	$this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8','confirmed'],
		]);

		$user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return $this->sendResponse(true, $user, 'User registered successfully');
    }

    public function logout()
    {
    	Auth::user()->currentAccessToken()->delete();
        return $this->sendResponse(true,[],'Successfully logged out'); 
    }

    public function user()
    {
        return $this->sendResponse(true, Auth::user());
    }
}
