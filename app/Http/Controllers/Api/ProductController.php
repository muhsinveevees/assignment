<?php

namespace App\Http\Controllers\Api;

use App\CsvUpload;
use App\Helpers\ApiHelper;
use App\Http\Controllers\Controller;
use App\Imports\ProductsImport;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    use ApiHelper;


    public function index()
    {
    	$product = Product::get();

    	return $this->sendResponse(true, $product);
    }

    public function upload(Request $request)
    {
    	$this->validate($request, [
            'upload_file' => ['required', 'file', 'mimes:csv,txt']
        ],
        [
        	 'upload_file.mimes' => "The upload file must be a file of type: csv"
        ]);


        $path = Storage::disk('public')->putFileAs('csv',$request->file('upload_file'),Str::uuid().'-'.$request->file('upload_file')->getClientOriginalName(),'public');
        $csv_upload = CsvUpload::create([
            'path' => $path,
            'header' => $request->header == 'true' ? 1 : 0
        ]);

        $data = array_map('str_getcsv', file(storage_path('app/public/'.$path)) );
        $csv_data = $request->header ? array_slice($data, 1, 2) : array_slice($data, 0, 2) ;

    	return $this->sendResponse(true, ['csv_upload'=>$csv_upload,'csv_data'=>$csv_data],'CSV imported succefully');
    }

    public function mapping(Request $request)
    {
        $this->validate($request, [
            'csv_id' => ['required'],
            'columns' => ['required']
        ],
        [
             'csv_id.required' => "No csv file data",
             'columns.required' => "No column mapping data"
        ]);
        $csv_upload = CsvUpload::find($request->csv_id);
        $data = array_map('str_getcsv', file(storage_path('app/public/'.$csv_upload->path)) );
        $csv_data = $csv_upload->header ? array_slice($data, 1) : $data;

        $columns = $request->columns;
        $d = array_map(function ($value) use ($columns)
        {
            $a = array_combine($columns, $value);
            $a['created_at']        = Carbon::now();
            $a['updated_at' ]       = Carbon::now();
         return $a;
        },$csv_data);

        $request = new Request([
            'row' => $d,
        ]);

        $this->validate($request, [
            'row.*.name'           => 'required|string',
            'row.*.price'          => 'required|numeric', 
            'row.*.ski'            => 'required|string',
        ]);

        Product::insert($d);
        Storage::disk('public')->delete($csv_upload->path);
        $csv_upload->delete();
        return $this->sendResponse(true, [], 'Product data Inserted Successfully');
    }
}
