<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->name('api.')->group( function() {

	Route::post('login','AuthController@login')->name('login');
	Route::post('register','AuthController@register')->name('register');

	Route::middleware('auth:sanctum')->group( function() {
		Route::post('user','AuthController@user')->name('user');
		Route::post('logout','AuthController@logout')->name('logout');

		Route::get('products','ProductController@index')->name('products.index');
		Route::post('products/upload','ProductController@upload')->name('products.upload');
		Route::post('products/mapping','ProductController@mapping')->name('products.mapping');
	});

});